# Merge Box


## German:

Merge Box ist ein Spiel bei dem man Kisten verbinden muss damit eine bessere entsteht. Ziel des Spiels ist es den Rekord zu knacken.


## English:

Merge Box is a game where you have to connect boxes to make a better one. The aim of the game is to break the record.
