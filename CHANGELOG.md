# Changelog

### [1.0:](https://gitlab.com/games120/merge_box/-/tree/main/Merge_Box1.0)

Spiel wurde erstellt.


### [1.0.1:](https://gitlab.com/games120/merge_box/-/tree/main/Merge_Box1.0.1)

Es wurden mehr Geschwindigkeits Upgrades hinzugefuegt, weil es davon zuwenig gab.


### [1.0.2:](https://gitlab.com/games120/merge_box/-/tree/main/Merge_Box1.0.2)

Omegas werden nun auch angezeigt, wenn der Shop offen ist, aber das Spiel ist wie vorher pausiert.
