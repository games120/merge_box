#!/usr/bin/python3

import os
import sys

# Try imports

no_importerror = True

try:
    import time

except ModuleNotFoundError:
    os.system("pip3 install time")
    no_importerror = False

try:
    import pygame
    from pygame.locals import *

except ModuleNotFoundError:
    os.system("pip3 install pygame")
    no_importerror = False

try:
    import random

except ModuleNotFoundError:
    os.system("pip3 install random")

if no_importerror == True:
    try:
        
        # Intro
        
        print("\n\x1b[38;5;196mThis game is not my invention. The original game was programmed with Scratch by debiankaios. Link to original game: https://scratch.mit.edu/projects/502621608/ Link to debiankaios: https://scratch.mit.edu/users/debiankaios/\033[0m\n")
        print("\n\x1b[38;5;46mMusic in this game:\n\n   TheFatRat - Prelude\n   Kevin Macleod - Easy Lemon\033[0m\n")

        # Set Variables

        game_directory = os.path.dirname(os.path.abspath(__file__))

        developer_mode = False

        page = "main_menu"

        music = "prelude_thefatrat.mp3"

        max_fps = 60

        clock = pygame.time.Clock()

        click_cooldown_setting = 10
        click_cooldown = click_cooldown_setting

        shop = False
        coins = 0
        
        spawning_speed_level = 1
        spawning_speed_level_to_costs = {
            2: 50,
            3: 100,
            4: 150,
            5: 200,
            6: 250,
            7: 300,
            8: 350,
            9: 400,
            10: 450,
            11: 500,
            12: 600,
            13: 800,
            14: 1000
        }
        
        level_boxes_spawning = 1
        level_boxes_spawning_to_costs = {
            2: 400,
            3: 800,
            4: 1600,
            5: 3200,
            6: 6400,
            7: 12800,
            8: 25600,
            9: 51200,
            10: 102400
        }

        omega_1_list = []
        omega_2_list = []
        omega_triangle_list = []
        omega_box_list = []
        omega_v_list = []
        omega_a_list = []
        omega_b_list = []
        omega_8_list = []
        omega_c_list = []
        omega_d_list = []
        omega_e_list = []
        omega_plus_list = []
        omega_omega_1_list = []
        omega_omega_2_list = []
        omega_omega_3_list = []

        current_selectet_omega = ""
        current_selectet_omega_x = 0
        current_selectet_omega_y = 0
        
        pygame.init()
        screen = pygame.display.set_mode((900, 600))
        pygame.display.set_caption("Merge Box 1.0")
        pygame.display.set_icon(pygame.image.load(game_directory + "/textures/merge_box_icon.png"))
        pygame.mouse.set_visible(False)

        # Set Nodes
        
        minecraftevenings_size_25 = pygame.font.Font(game_directory + "/fonts/MinecraftEvenings-RBao.ttf", 25)
        minecraftevenings_size_15 = pygame.font.Font(game_directory + "/fonts/MinecraftEvenings-RBao.ttf", 15)
        titilliumwebbold_size_25 = pygame.font.Font(game_directory + "/fonts/TitilliumWeb-Bold.ttf", 25)

        this_game_is_not_my_invention = titilliumwebbold_size_25.render("This game is not my invention.", True, (255, 100, 100))
        the_real_game_was_programmed_with_scratch_by_debiankaios = titilliumwebbold_size_25.render("The original game was programmed with Scratch by debiankaios.", True, (255, 100, 100))
        link_to_original_game = titilliumwebbold_size_25.render("Link to original game: https://scratch.mit.edu/projects/502621608/", True, (255, 100, 100))
        link_to_debiankaios = titilliumwebbold_size_25.render("Link to debiankaios: https://scratch.mit.edu/users/debiankaios/", True, (255, 100, 100))

        main_menu_background = pygame.image.load(game_directory + "/textures/main_menu_background.png")
        main_menu_background = pygame.transform.scale(main_menu_background, (900, 600))

        cursor = pygame.image.load(game_directory + "/textures/cursor.png")
        cursor = pygame.transform.scale(cursor, (900, 600))

        start_button_image = pygame.image.load(game_directory + "/textures/start_button.png")
        start_button_image = pygame.transform.scale(start_button_image, (900, 600))

        background = pygame.image.load(game_directory + "/textures/background.png")
        background = pygame.transform.scale(background, (900, 600))

        change_music_button_image = pygame.image.load(game_directory + "/textures/change_music_button.png")
        change_music_button_image = pygame.transform.scale(change_music_button_image, (900, 600))

        shop_button_image = pygame.image.load(game_directory + "/textures/shop_button.png")
        shop_button_image = pygame.transform.scale(shop_button_image, (900, 600))

        close_button_image = pygame.image.load(game_directory + "/textures/close_button.png")
        close_button_image = pygame.transform.scale(close_button_image, (900, 600))

        shop_background_image = pygame.image.load(game_directory + "/textures/shop_background.png")
        shop_background_image = pygame.transform.scale(shop_background_image, (900, 600))

        faster_spawning_button_image = pygame.image.load(game_directory + "/textures/faster_spawning_button.png")
        faster_spawning_button_image = pygame.transform.scale(faster_spawning_button_image, (600, 400))

        spawn_higher_boxes_button_image = pygame.image.load(game_directory + "/textures/spawn_higher_boxes_button.png")
        spawn_higher_boxes_button_image = pygame.transform.scale(spawn_higher_boxes_button_image, (600, 400))

        give_away_coins_button_image = pygame.image.load(game_directory + "/textures/give_away_coins_button.png")
        give_away_coins_button_image = pygame.transform.scale(give_away_coins_button_image, (600, 400))
        give_away_coins_text = minecraftevenings_size_15.render("Comming soon...", True, (255, 255, 255))

        get_coins_button_image = pygame.image.load(game_directory + "/textures/get_coins_button.png")
        get_coins_button_image = pygame.transform.scale(get_coins_button_image, (600, 400))
        get_coins_text = minecraftevenings_size_15.render("Comming soon...", True, (255, 255, 255))

        omega_1_image = pygame.image.load(game_directory + "/textures/omegas/omega_1.png")
        omega_1_clicked_image = pygame.transform.scale(omega_1_image, (680, 480))
        omega_1_image = pygame.transform.scale(omega_1_image, (600, 400))

        omega_2_image = pygame.image.load(game_directory + "/textures/omegas/omega_2.png")
        omega_2_clicked_image = pygame.transform.scale(omega_2_image, (680, 480))
        omega_2_image = pygame.transform.scale(omega_2_image, (600, 400))

        omega_triangle_image = pygame.image.load(game_directory + "/textures/omegas/omega_triangle.png")
        omega_triangle_clicked_image = pygame.transform.scale(omega_triangle_image, (680, 480))
        omega_triangle_image = pygame.transform.scale(omega_triangle_image, (600, 400))

        omega_box_image = pygame.image.load(game_directory + "/textures/omegas/omega_box.png")
        omega_box_clicked_image = pygame.transform.scale(omega_box_image, (680, 480))
        omega_box_image = pygame.transform.scale(omega_box_image, (600, 400))

        omega_v_image = pygame.image.load(game_directory + "/textures/omegas/omega_v.png")
        omega_v_clicked_image = pygame.transform.scale(omega_v_image, (680, 480))
        omega_v_image = pygame.transform.scale(omega_v_image, (600, 400))

        omega_a_image = pygame.image.load(game_directory + "/textures/omegas/omega_a.png")
        omega_a_clicked_image = pygame.transform.scale(omega_a_image, (680, 480))
        omega_a_image = pygame.transform.scale(omega_a_image, (600, 400))

        omega_b_image = pygame.image.load(game_directory + "/textures/omegas/omega_b.png")
        omega_b_clicked_image = pygame.transform.scale(omega_b_image, (680, 480))
        omega_b_image = pygame.transform.scale(omega_b_image, (600, 400))

        omega_8_image = pygame.image.load(game_directory + "/textures/omegas/omega_8.png")
        omega_8_clicked_image = pygame.transform.scale(omega_8_image, (680, 480))
        omega_8_image = pygame.transform.scale(omega_8_image, (600, 400))

        omega_c_image = pygame.image.load(game_directory + "/textures/omegas/omega_c.png")
        omega_c_clicked_image = pygame.transform.scale(omega_c_image, (680, 480))
        omega_c_image = pygame.transform.scale(omega_c_image, (600, 400))

        omega_d_image = pygame.image.load(game_directory + "/textures/omegas/omega_d.png")
        omega_d_clicked_image = pygame.transform.scale(omega_d_image, (680, 480))
        omega_d_image = pygame.transform.scale(omega_d_image, (600, 400))
        
        omega_e_image = pygame.image.load(game_directory + "/textures/omegas/omega_e.png")
        omega_e_clicked_image = pygame.transform.scale(omega_e_image, (680, 480))
        omega_e_image = pygame.transform.scale(omega_e_image, (600, 400))

        omega_plus_image = pygame.image.load(game_directory + "/textures/omegas/omega_plus.png")
        omega_plus_clicked_image = pygame.transform.scale(omega_plus_image, (680, 480))
        omega_plus_image = pygame.transform.scale(omega_plus_image, (600, 400))

        omega_omega_1_image = pygame.image.load(game_directory + "/textures/omegas/omega_omega_1.png")
        omega_omega_1_clicked_image = pygame.transform.scale(omega_omega_1_image, (680, 480))
        omega_omega_1_image = pygame.transform.scale(omega_omega_1_image, (600, 400))

        omega_omega_2_image = pygame.image.load(game_directory + "/textures/omegas/omega_omega_2.png")
        omega_omega_2_clicked_image = pygame.transform.scale(omega_omega_2_image, (680, 480))
        omega_omega_2_image = pygame.transform.scale(omega_omega_2_image, (600, 400))

        omega_omega_3_image = pygame.image.load(game_directory + "/textures/omegas/omega_omega_3.png")
        omega_omega_3_image = pygame.transform.scale(omega_omega_3_image, (600, 400))

        # functions

        # exit

        def exit_game():
            print("Program closed.")
            pygame.quit()
            sys.exit()
        
        # Buttons

        def start_button():
            if cursor_x > 279 and cursor_x < 615:
                if cursor_y > 209 and cursor_y < 352:
                    print("Let's Go!")
                    
                    global page
                    page = "ingame"
                    
                    pygame.mixer.music.load(game_directory + "/sounds/" + music)
                    pygame.mixer.music.play(-1)

                    global click_cooldown
                    click_cooldown = click_cooldown_setting

                    global spawn_cooldown
                    spawn_cooldown = level_boxes_spawning * 40 - spawning_speed_level * 20

        def change_music_button():
            if cursor_x > 20 and cursor_x < 199:
                if cursor_y > 39 and cursor_y < 98:
                    global music

                    if developer_mode == True:
                        if music == "prelude_thefatrat.mp3":
                            music = "easy_lemon_kevin_macleod.mp3"
                            print("Music changed to Easy Lemon by Kevin Macleod.")

                        elif music == "easy_lemon_kevin_macleod.mp3":
                            music = "prelude_thefatrat.mp3"
                            print("Music changed to Prelude by TheFatRat.")

                    else:
                        if music == "prelude_thefatrat.mp3":
                            music = "easy_lemon_kevin_macleod.mp3"

                        elif music == "easy_lemon_kevin_macleod.mp3":
                            music = "prelude_thefatrat.mp3"

                    global click_cooldown
                    click_cooldown = click_cooldown_setting

        def shop_button():
            if cursor_x > 739 and cursor_x < 889:
                if cursor_y > 529 and cursor_y < 588:
                    global shop
                    shop = True

                    global click_cooldown
                    click_cooldown = click_cooldown_setting

        def close_shop_button():
            if cursor_x > 869 and cursor_y > 569:
                global shop
                shop = False

                global click_cooldown
                click_cooldown = click_cooldown_setting

        def faster_spawning_button():
            global coins
            global spawning_speed_level
            global click_cooldown

            if cursor_x > 579 and cursor_x < 734:
                if cursor_y > 9 and cursor_y < 82:
                    if spawning_speed_level < 14:
                        if coins > spawning_speed_level_to_costs[spawning_speed_level + 1] -1:
                            coins -= spawning_speed_level_to_costs[spawning_speed_level + 1]
                            
                            spawning_speed_level += 1

                            click_cooldown = click_cooldown_setting

        def spawn_higher_boxes_button():
            global coins
            global level_boxes_spawning
            global click_cooldown

            if cursor_x > 579 and cursor_x < 721:
                if cursor_y > 91 and cursor_y < 194:
                    if level_boxes_spawning < 10:
                        if coins > level_boxes_spawning_to_costs[level_boxes_spawning + 1] -1:
                            coins -= level_boxes_spawning_to_costs[level_boxes_spawning + 1]

                            level_boxes_spawning += 1

                            click_cooldown = click_cooldown_setting

        # Command Line

        def command_line():
                command = input(">>> ")
                command_list = command.split()

                if len(command_list) == 1:
                    if command_list[0] == "help":
                        print("\nCommands:\n")
                        print("help - Shows this here.")
                        print("developer_mode [True/False] - Turns the developer mode On/Off.")

                if len(command_list) == 2:
                    if command_list[0] == "developer_mode":
                        global developer_mode

                        if command_list[1] == "True":
                            developer_mode = True
                            print("\x1b[38;5;57mDeveloper mode is now enabled.\033[0m")

                        if command_list[1] == "False":
                            developer_mode = False
                            print("\x1b[38;5;57mDeveloper mode is now disabled.\033[0m")
                        
                        if not command_list[1] == "True" and not command_list[1] == "False":
                            print("Only True or False is accepted.")    

        def music_text_script():
            global music_text

            if music == "prelude_thefatrat.mp3":
                music_text = minecraftevenings_size_25.render("Music: Prelude - TheFatRat", True, (255, 255, 255))

            if music == "easy_lemon_kevin_macleod.mp3":
                music_text = minecraftevenings_size_25.render("Music: Easy Lemon - Kevin Macleod", True, (255, 255, 255))

        def ctrl_slash():
            if pygame.key.get_pressed()[K_LCTRL]:
                if pygame.key.get_pressed()[K_SLASH]:
                    command_line()

        def developer_mode_script():
            if developer_mode == True:
                if pygame.mouse.get_pressed()[0]:
                    print(pygame.mouse.get_pos())

        def cursor_script():
            screen.blit(cursor, (cursor_x, cursor_y))

        def ctrl_c():
            if pygame.key.get_pressed()[K_LCTRL]:
                if pygame.key.get_pressed()[K_c]:
                    exit_game()

        def click_cooldown_script():
            global click_cooldown
            
            if click_cooldown > 0:
                click_cooldown -= 1

        def display_coins_script():
            global display_coins

            display_coins = minecraftevenings_size_25.render("Your Coins: " + str(coins), True, (255, 255, 255))

        def faster_spawning_costs_script():
            global faster_spawning_costs_text

            if spawning_speed_level < 14:
                faster_spawning_costs_text = minecraftevenings_size_15.render("Costs: " + str(spawning_speed_level_to_costs[spawning_speed_level + 1]), True, (255, 255, 255))

            else:
                faster_spawning_costs_text = minecraftevenings_size_15.render("max", True, (255, 255, 255))


        def spawn_higher_boxes_costs_script():
            global spawn_higher_boxes_costs_text

            if level_boxes_spawning < 10:
                spawn_higher_boxes_costs_text = minecraftevenings_size_15.render("Costs: " + str(level_boxes_spawning_to_costs[level_boxes_spawning + 1]), True, (255, 255, 255))

            else:
                spawn_higher_boxes_costs_text = minecraftevenings_size_15.render("max", True, (255, 255, 255))

        def spawn_omega_script():
            if level_boxes_spawning < 11:
                x = random.randrange(0, 861)
                y = random.randrange(0, 561)

                if not {"x": x, "y": y} in omega_1_list:
                    if not {"x": x, "y": y} in omega_2_list:
                        if not {"x": x, "y": y} in omega_triangle_list:
                            if not {"x": x, "y": y} in omega_box_list:
                                if not {"x": x, "y": y} in omega_v_list:
                                    if not{"x": x, "y": y} in omega_a_list:
                                        if not {"x": x, "y": y} in omega_b_list:
                                            if not {"x": x, "y": y} in omega_8_list:
                                                if not {"x": x, "y": y} in omega_c_list:
                                                    if not {"x": x, "y": y} in omega_d_list:
                                                        if not {"x": x, "y": y} in omega_e_list:
                                                            if not {"x": x, "y": y} in omega_plus_list:
                                                                if not {"x": x, "y": y} in omega_omega_1_list:
                                                                    if not {"x": x, "y": y} in omega_omega_2_list:
                                                                        if not {"x": x, "y": y} in omega_omega_3_list:
                                                                            if level_boxes_spawning == 1:
                                                                                omega_1_list.append({"x": x, "y": y})

                                                                            elif level_boxes_spawning == 2:
                                                                                omega_2_list.append({"x": x, "y": y})

                                                                            elif level_boxes_spawning == 3:
                                                                                omega_triangle_list.append({"x": x, "y": y})

                                                                            elif level_boxes_spawning == 4:
                                                                                omega_box_list.append({"x": x, "y": y})

                                                                            elif level_boxes_spawning == 5:
                                                                                omega_v_list.append({"x": x, "y": y})

                                                                            elif level_boxes_spawning == 6:
                                                                                omega_a_list.append({"x": x, "y": y})

                                                                            elif level_boxes_spawning == 7:
                                                                                omega_b_list.append({"x": x, "y": y})

                                                                            elif level_boxes_spawning == 8:
                                                                                omega_8_list.append({"x": x, "y": y})

                                                                            elif level_boxes_spawning == 9:
                                                                                omega_c_list.append({"x": x, "y": y})

                                                                            elif level_boxes_spawning == 10:
                                                                                omega_d_list.append({"x": x, "y": y})

        def display_omegas_script():
            for omega_1_in_list in omega_1_list:
                    if not current_selectet_omega == "omega_1" or not omega_1_in_list["x"] == current_selectet_omega_x or not omega_1_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_1_image, (omega_1_in_list["x"], omega_1_in_list["y"]))

            for omega_2_in_list in omega_2_list:
                    if not current_selectet_omega == "omega_2" or not omega_2_in_list["x"] == current_selectet_omega_x or not omega_2_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_2_image, (omega_2_in_list["x"], omega_2_in_list["y"]))

            for omega_triangle_in_list in omega_triangle_list:
                    if not current_selectet_omega == "omega_triangle" or not omega_triangle_in_list["x"] == current_selectet_omega_x or not omega_triangle_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_triangle_image, (omega_triangle_in_list["x"], omega_triangle_in_list["y"]))

            for omega_box_in_list in omega_box_list:
                    if not current_selectet_omega == "omega_box" or not omega_box_in_list["x"] == current_selectet_omega_x or not omega_box_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_box_image, (omega_box_in_list["x"], omega_box_in_list["y"]))

            for omega_v_in_list in omega_v_list:
                    if not current_selectet_omega == "omega_v" or not omega_v_in_list["x"] == current_selectet_omega_x or not omega_v_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_v_image, (omega_v_in_list["x"], omega_v_in_list["y"]))

            for omega_a_in_list in omega_a_list:
                    if not current_selectet_omega == "omega_a" or not omega_a_in_list["x"] == current_selectet_omega_x or not omega_a_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_a_image, (omega_a_in_list["x"], omega_a_in_list["y"]))

            for omega_b_in_list in omega_b_list:
                    if not current_selectet_omega == "omega_b" or not omega_b_in_list["x"] == current_selectet_omega_x or not omega_b_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_b_image, (omega_b_in_list["x"], omega_b_in_list["y"]))

            for omega_8_in_list in omega_8_list:
                    if not current_selectet_omega == "omega_8" or not omega_8_in_list["x"] == current_selectet_omega_x or not omega_8_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_8_image, (omega_8_in_list["x"], omega_8_in_list["y"]))

            for omega_c_in_list in omega_c_list:
                    if not current_selectet_omega == "omega_c" or not omega_c_in_list["x"] == current_selectet_omega_x or not omega_c_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_c_image, (omega_c_in_list["x"], omega_c_in_list["y"]))

            for omega_d_in_list in omega_d_list:
                    if not current_selectet_omega == "omega_d" or not omega_d_in_list["x"] == current_selectet_omega_x or not omega_d_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_d_image, (omega_d_in_list["x"], omega_d_in_list["y"]))

            for omega_e_in_list in omega_e_list:
                    if not current_selectet_omega == "omega_e" or not omega_e_in_list["x"] == current_selectet_omega_x or not omega_e_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_e_image, (omega_e_in_list["x"], omega_e_in_list["y"]))

            for omega_plus_in_list in omega_plus_list:
                    if not current_selectet_omega == "omega_plus" or not omega_plus_in_list["x"] == current_selectet_omega_x or not omega_plus_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_plus_image, (omega_plus_in_list["x"], omega_plus_in_list["y"]))

            for omega_omega_1_in_list in omega_omega_1_list:
                    if not current_selectet_omega == "omega_omega_1" or not omega_omega_1_in_list["x"] == current_selectet_omega_x or not omega_omega_1_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_omega_1_image, (omega_omega_1_in_list["x"], omega_omega_1_in_list["y"]))

            for omega_omega_2_in_list in omega_omega_2_list:
                    if not current_selectet_omega == "omega_omega_2" or not omega_omega_2_in_list["x"] == current_selectet_omega_x or not omega_omega_2_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_omega_2_image, (omega_omega_2_in_list["x"], omega_omega_2_in_list["y"]))
            
            for omega_omega_3_in_list in omega_omega_3_list:
                    if not current_selectet_omega == "omega_omega_3" or not omega_omega_3_in_list["x"] == current_selectet_omega_x or not omega_omega_3_in_list["y"] == current_selectet_omega_y:
                        screen.blit(omega_omega_3_image, (omega_omega_3_in_list["x"], omega_omega_3_in_list["y"]))

            if current_selectet_omega == "omega_1":
                screen.blit(omega_1_clicked_image, (current_selectet_omega_x, current_selectet_omega_y))

            elif current_selectet_omega == "omega_2":
                screen.blit(omega_2_clicked_image, (current_selectet_omega_x, current_selectet_omega_y))

            elif current_selectet_omega == "omega_triangle":
                screen.blit(omega_triangle_clicked_image, (current_selectet_omega_x, current_selectet_omega_y))
        
            elif current_selectet_omega == "omega_box":
                screen.blit(omega_box_clicked_image, (current_selectet_omega_x, current_selectet_omega_y))

            elif current_selectet_omega == "omega_v":
                screen.blit(omega_v_clicked_image, (current_selectet_omega_x, current_selectet_omega_y))

            elif current_selectet_omega == "omega_a":
                screen.blit(omega_a_clicked_image, (current_selectet_omega_x, current_selectet_omega_y))

            elif current_selectet_omega == "omega_b":
                screen.blit(omega_b_clicked_image, (current_selectet_omega_x, current_selectet_omega_y))

            elif current_selectet_omega == "omega_8":
                screen.blit(omega_8_clicked_image, (current_selectet_omega_x, current_selectet_omega_y))

            elif current_selectet_omega == "omega_c":
                screen.blit(omega_c_clicked_image, (current_selectet_omega_x, current_selectet_omega_y))

            elif current_selectet_omega == "omega_d":
                screen.blit(omega_d_clicked_image, (current_selectet_omega_x, current_selectet_omega_y))

            elif current_selectet_omega == "omega_e":
                screen.blit(omega_e_clicked_image, (current_selectet_omega_x, current_selectet_omega_y))

            elif current_selectet_omega == "omega_plus":
                screen.blit(omega_plus_clicked_image, (current_selectet_omega_x, current_selectet_omega_y))

            elif current_selectet_omega == "omega_omega_1":
                screen.blit(omega_omega_1_clicked_image, (current_selectet_omega_x, current_selectet_omega_y))

            elif current_selectet_omega == "omega_omage_2":
                screen.blit(omega_omega_2_clicked_image, (current_selectet_omega_x, current_selectet_omega_y))

        def box_click():
            global current_selectet_omega
            global current_selectet_omega_x
            global current_selectet_omega_y
            global click_cooldown
            
            for omega_1_coordinates_in_list in omega_1_list:
                if not current_selectet_omega == "omega_1" or not omega_1_coordinates_in_list["x"] == current_selectet_omega_x or not omega_1_coordinates_in_list["y"] == current_selectet_omega_y:
                    if cursor_x > omega_1_coordinates_in_list["x"] - 1 and cursor_x < omega_1_coordinates_in_list["x"] + 41:
                        if cursor_y > omega_1_coordinates_in_list["y"] - 1 and cursor_y < omega_1_coordinates_in_list["y"] + 41:
                            if current_selectet_omega == "omega_1":
                                omega_1_list.remove({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                omega_1_list.remove({"x": omega_1_coordinates_in_list["x"], "y": omega_1_coordinates_in_list["y"]})
                                omega_2_list.append({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                
                                current_selectet_omega = ""
                                current_selectet_omega_x = 0
                                current_selectet_omega_y = 0

                            else:
                                current_selectet_omega = "omega_1"
                                current_selectet_omega_x = omega_1_coordinates_in_list["x"]
                                current_selectet_omega_y = omega_1_coordinates_in_list["y"]

                            click_cooldown = click_cooldown_setting
                            break

            for omega_2_coordinates_in_list in omega_2_list:
                if not current_selectet_omega == "omega_2" or not omega_2_coordinates_in_list["x"] == current_selectet_omega_x or not omega_2_coordinates_in_list["y"] == current_selectet_omega_y:
                    if cursor_x > omega_2_coordinates_in_list["x"] - 1 and cursor_x < omega_2_coordinates_in_list["x"] + 41:
                        if cursor_y > omega_2_coordinates_in_list["y"] - 1 and cursor_y < omega_2_coordinates_in_list["y"] + 41:
                            if current_selectet_omega == "omega_2":
                                omega_2_list.remove({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                omega_2_list.remove({"x": omega_2_coordinates_in_list["x"], "y": omega_2_coordinates_in_list["y"]})
                                omega_triangle_list.append({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                
                                current_selectet_omega = ""
                                current_selectet_omega_x = 0
                                current_selectet_omega_y = 0

                            else:
                                current_selectet_omega = "omega_2"
                                current_selectet_omega_x = omega_2_coordinates_in_list["x"]
                                current_selectet_omega_y = omega_2_coordinates_in_list["y"]

                            click_cooldown = click_cooldown_setting
                            break

            for omega_triangle_coordinates_in_list in omega_triangle_list:
                if not current_selectet_omega == "omega_triangle" or not omega_triangle_coordinates_in_list["x"] == current_selectet_omega_x or not omega_triangle_coordinates_in_list["y"] == current_selectet_omega_y:
                    if cursor_x > omega_triangle_coordinates_in_list["x"] - 1 and cursor_x < omega_triangle_coordinates_in_list["x"] + 41:
                        if cursor_y > omega_triangle_coordinates_in_list["y"] - 1 and cursor_y < omega_triangle_coordinates_in_list["y"] + 41:
                            if current_selectet_omega == "omega_triangle":
                                omega_triangle_list.remove({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                omega_triangle_list.remove({"x": omega_triangle_coordinates_in_list["x"], "y": omega_triangle_coordinates_in_list["y"]})
                                omega_box_list.append({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                
                                current_selectet_omega = ""
                                current_selectet_omega_x = 0
                                current_selectet_omega_y = 0

                            else:
                                current_selectet_omega = "omega_triangle"
                                current_selectet_omega_x = omega_triangle_coordinates_in_list["x"]
                                current_selectet_omega_y = omega_triangle_coordinates_in_list["y"]

                            click_cooldown = click_cooldown_setting
                            break

            for omega_box_coordinates_in_list in omega_box_list:
                if not current_selectet_omega == "omega_box" or not omega_box_coordinates_in_list["x"] == current_selectet_omega_x or not omega_box_coordinates_in_list["y"] == current_selectet_omega_y:
                    if cursor_x > omega_box_coordinates_in_list["x"] - 1 and cursor_x < omega_box_coordinates_in_list["x"] + 41:
                        if cursor_y > omega_box_coordinates_in_list["y"] - 1 and cursor_y < omega_box_coordinates_in_list["y"] + 41:
                            if current_selectet_omega == "omega_box":
                                omega_box_list.remove({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                omega_box_list.remove({"x": omega_box_coordinates_in_list["x"], "y": omega_box_coordinates_in_list["y"]})
                                omega_v_list.append({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                
                                current_selectet_omega = ""
                                current_selectet_omega_x = 0
                                current_selectet_omega_y = 0

                            else:
                                current_selectet_omega = "omega_box"
                                current_selectet_omega_x = omega_box_coordinates_in_list["x"]
                                current_selectet_omega_y = omega_box_coordinates_in_list["y"]

                            click_cooldown = click_cooldown_setting
                            break

            for omega_v_coordinates_in_list in omega_v_list:
                if not current_selectet_omega == "omega_v" or not omega_v_coordinates_in_list["x"] == current_selectet_omega_x or not omega_v_coordinates_in_list["y"] == current_selectet_omega_y:
                    if cursor_x > omega_v_coordinates_in_list["x"] - 1 and cursor_x < omega_v_coordinates_in_list["x"] + 41:
                        if cursor_y > omega_v_coordinates_in_list["y"] - 1 and cursor_y < omega_v_coordinates_in_list["y"] + 41:
                            if current_selectet_omega == "omega_v":
                                omega_v_list.remove({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                omega_v_list.remove({"x": omega_v_coordinates_in_list["x"], "y": omega_v_coordinates_in_list["y"]})
                                omega_a_list.append({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                
                                current_selectet_omega = ""
                                current_selectet_omega_x = 0
                                current_selectet_omega_y = 0

                            else:
                                current_selectet_omega = "omega_v"
                                current_selectet_omega_x = omega_v_coordinates_in_list["x"]
                                current_selectet_omega_y = omega_v_coordinates_in_list["y"]

                            click_cooldown = click_cooldown_setting
                            break

            for omega_a_coordinates_in_list in omega_a_list:
                if not current_selectet_omega == "omega_a" or not omega_a_coordinates_in_list["x"] == current_selectet_omega_x or not omega_a_coordinates_in_list["y"] == current_selectet_omega_y:
                    if cursor_x > omega_a_coordinates_in_list["x"] - 1 and cursor_x < omega_a_coordinates_in_list["x"] + 41:
                        if cursor_y > omega_a_coordinates_in_list["y"] - 1 and cursor_y < omega_a_coordinates_in_list["y"] + 41:
                            if current_selectet_omega == "omega_a":
                                omega_a_list.remove({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                omega_a_list.remove({"x": omega_a_coordinates_in_list["x"], "y": omega_a_coordinates_in_list["y"]})
                                omega_b_list.append({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                
                                current_selectet_omega = ""
                                current_selectet_omega_x = 0
                                current_selectet_omega_y = 0

                            else:
                                current_selectet_omega = "omega_a"
                                current_selectet_omega_x = omega_a_coordinates_in_list["x"]
                                current_selectet_omega_y = omega_a_coordinates_in_list["y"]

                            click_cooldown = click_cooldown_setting
                            break

            for omega_b_coordinates_in_list in omega_b_list:
                if not current_selectet_omega == "omega_b" or not omega_b_coordinates_in_list["x"] == current_selectet_omega_x or not omega_b_coordinates_in_list["y"] == current_selectet_omega_y:
                    if cursor_x > omega_b_coordinates_in_list["x"] - 1 and cursor_x < omega_b_coordinates_in_list["x"] + 41:
                        if cursor_y > omega_b_coordinates_in_list["y"] - 1 and cursor_y < omega_b_coordinates_in_list["y"] + 41:
                            if current_selectet_omega == "omega_b":
                                omega_b_list.remove({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                omega_b_list.remove({"x": omega_b_coordinates_in_list["x"], "y": omega_b_coordinates_in_list["y"]})
                                omega_8_list.append({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                
                                current_selectet_omega = ""
                                current_selectet_omega_x = 0
                                current_selectet_omega_y = 0

                            else:
                                current_selectet_omega = "omega_b"
                                current_selectet_omega_x = omega_b_coordinates_in_list["x"]
                                current_selectet_omega_y = omega_b_coordinates_in_list["y"]

                            click_cooldown = click_cooldown_setting
                            break

            for omega_8_coordinates_in_list in omega_8_list:
                if not current_selectet_omega == "omega_8" or not omega_8_coordinates_in_list["x"] == current_selectet_omega_x or not omega_8_coordinates_in_list["y"] == current_selectet_omega_y:
                    if cursor_x > omega_8_coordinates_in_list["x"] - 1 and cursor_x < omega_8_coordinates_in_list["x"] + 41:
                        if cursor_y > omega_8_coordinates_in_list["y"] - 1 and cursor_y < omega_8_coordinates_in_list["y"] + 41:
                            if current_selectet_omega == "omega_8":
                                omega_8_list.remove({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                omega_8_list.remove({"x": omega_8_coordinates_in_list["x"], "y": omega_8_coordinates_in_list["y"]})
                                omega_c_list.append({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                
                                current_selectet_omega = ""
                                current_selectet_omega_x = 0
                                current_selectet_omega_y = 0

                            else:
                                current_selectet_omega = "omega_8"
                                current_selectet_omega_x = omega_8_coordinates_in_list["x"]
                                current_selectet_omega_y = omega_8_coordinates_in_list["y"]

                            click_cooldown = click_cooldown_setting
                            break

            for omega_c_coordinates_in_list in omega_c_list:
                if not current_selectet_omega == "omega_c" or not omega_c_coordinates_in_list["x"] == current_selectet_omega_x or not omega_c_coordinates_in_list["y"] == current_selectet_omega_y:
                    if cursor_x > omega_c_coordinates_in_list["x"] - 1 and cursor_x < omega_c_coordinates_in_list["x"] + 41:
                        if cursor_y > omega_c_coordinates_in_list["y"] - 1 and cursor_y < omega_c_coordinates_in_list["y"] + 41:
                            if current_selectet_omega == "omega_c":
                                omega_c_list.remove({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                omega_c_list.remove({"x": omega_c_coordinates_in_list["x"], "y": omega_c_coordinates_in_list["y"]})
                                omega_d_list.append({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                
                                current_selectet_omega = ""
                                current_selectet_omega_x = 0
                                current_selectet_omega_y = 0

                            else:
                                current_selectet_omega = "omega_c"
                                current_selectet_omega_x = omega_c_coordinates_in_list["x"]
                                current_selectet_omega_y = omega_c_coordinates_in_list["y"]

                            click_cooldown = click_cooldown_setting
                            break

            for omega_d_coordinates_in_list in omega_d_list:
                if not current_selectet_omega == "omega_d" or not omega_d_coordinates_in_list["x"] == current_selectet_omega_x or not omega_d_coordinates_in_list["y"] == current_selectet_omega_y:
                    if cursor_x > omega_d_coordinates_in_list["x"] - 1 and cursor_x < omega_d_coordinates_in_list["x"] + 41:
                        if cursor_y > omega_d_coordinates_in_list["y"] - 1 and cursor_y < omega_d_coordinates_in_list["y"] + 41:
                            if current_selectet_omega == "omega_d":
                                omega_d_list.remove({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                omega_d_list.remove({"x": omega_d_coordinates_in_list["x"], "y": omega_d_coordinates_in_list["y"]})
                                omega_e_list.append({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                
                                current_selectet_omega = ""
                                current_selectet_omega_x = 0
                                current_selectet_omega_y = 0

                            else:
                                current_selectet_omega = "omega_d"
                                current_selectet_omega_x = omega_d_coordinates_in_list["x"]
                                current_selectet_omega_y = omega_d_coordinates_in_list["y"]

                            click_cooldown = click_cooldown_setting
                            break

            for omega_e_coordinates_in_list in omega_e_list:
                if not current_selectet_omega == "omega_e" or not omega_e_coordinates_in_list["x"] == current_selectet_omega_x or not omega_e_coordinates_in_list["y"] == current_selectet_omega_y:
                    if cursor_x > omega_e_coordinates_in_list["x"] - 1 and cursor_x < omega_e_coordinates_in_list["x"] + 41:
                        if cursor_y > omega_e_coordinates_in_list["y"] - 1 and cursor_y < omega_e_coordinates_in_list["y"] + 41:
                            if current_selectet_omega == "omega_e":
                                omega_e_list.remove({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                omega_e_list.remove({"x": omega_e_coordinates_in_list["x"], "y": omega_e_coordinates_in_list["y"]})
                                omega_plus_list.append({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                
                                current_selectet_omega = ""
                                current_selectet_omega_x = 0
                                current_selectet_omega_y = 0

                            else:
                                current_selectet_omega = "omega_e"
                                current_selectet_omega_x = omega_e_coordinates_in_list["x"]
                                current_selectet_omega_y = omega_e_coordinates_in_list["y"]

                            click_cooldown = click_cooldown_setting
                            break

            for omega_plus_coordinates_in_list in omega_plus_list:
                if not current_selectet_omega == "omega_plus" or not omega_plus_coordinates_in_list["x"] == current_selectet_omega_x or not omega_plus_coordinates_in_list["y"] == current_selectet_omega_y:
                    if cursor_x > omega_plus_coordinates_in_list["x"] - 1 and cursor_x < omega_plus_coordinates_in_list["x"] + 41:
                        if cursor_y > omega_plus_coordinates_in_list["y"] - 1 and cursor_y < omega_plus_coordinates_in_list["y"] + 41:
                            if current_selectet_omega == "omega_plus":
                                omega_plus_list.remove({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                omega_plus_list.remove({"x": omega_plus_coordinates_in_list["x"], "y": omega_plus_coordinates_in_list["y"]})
                                omega_omega_1_list.append({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                
                                current_selectet_omega = ""
                                current_selectet_omega_x = 0
                                current_selectet_omega_y = 0

                            else:
                                current_selectet_omega = "omega_plus"
                                current_selectet_omega_x = omega_plus_coordinates_in_list["x"]
                                current_selectet_omega_y = omega_plus_coordinates_in_list["y"]

                            click_cooldown = click_cooldown_setting
                            break

            for omega_omega_1_coordinates_in_list in omega_omega_1_list:
                if not current_selectet_omega == "omega_omega_1" or not omega_omega_1_coordinates_in_list["x"] == current_selectet_omega_x or not omega_omega_1_coordinates_in_list["y"] == current_selectet_omega_y:
                    if cursor_x > omega_omega_1_coordinates_in_list["x"] - 1 and cursor_x < omega_omega_1_coordinates_in_list["x"] + 41:
                        if cursor_y > omega_omega_1_coordinates_in_list["y"] - 1 and cursor_y < omega_omega_1_coordinates_in_list["y"] + 41:
                            if current_selectet_omega == "omega_omega_1":
                                omega_omega_1_list.remove({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                omega_omega_1_list.remove({"x": omega_omega_1_coordinates_in_list["x"], "y": omega_omega_1_coordinates_in_list["y"]})
                                omega_omega_2_list.append({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                
                                current_selectet_omega = ""
                                current_selectet_omega_x = 0
                                current_selectet_omega_y = 0

                            else:
                                current_selectet_omega = "omega_omega_1"
                                current_selectet_omega_x = omega_omega_1_coordinates_in_list["x"]
                                current_selectet_omega_y = omega_omega_1_coordinates_in_list["y"]

                            click_cooldown = click_cooldown_setting
                            break

            for omega_omega_2_coordinates_in_list in omega_omega_2_list:
                if not current_selectet_omega == "omega_omega_2" or not omega_omega_2_coordinates_in_list["x"] == current_selectet_omega_x or not omega_omega_2_coordinates_in_list["y"] == current_selectet_omega_y:
                    if cursor_x > omega_omega_2_coordinates_in_list["x"] - 1 and cursor_x < omega_omega_2_coordinates_in_list["x"] + 41:
                        if cursor_y > omega_omega_2_coordinates_in_list["y"] - 1 and cursor_y < omega_omega_2_coordinates_in_list["y"] + 41:
                            if current_selectet_omega == "omega_omega_2":
                                omega_omega_2_list.remove({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                omega_omega_2_list.remove({"x": omega_omega_2_coordinates_in_list["x"], "y": omega_omega_2_coordinates_in_list["y"]})
                                omega_omega_3_list.append({"x": current_selectet_omega_x, "y": current_selectet_omega_y})
                                
                                current_selectet_omega = ""
                                current_selectet_omega_x = 0
                                current_selectet_omega_y = 0

                            else:
                                current_selectet_omega = "omega_omega_2"
                                current_selectet_omega_x = omega_omega_2_coordinates_in_list["x"]
                                current_selectet_omega_y = omega_omega_2_coordinates_in_list["y"]

                            click_cooldown = click_cooldown_setting
                            break

        # pages

        def main_menu_script():
            screen.blit(main_menu_background, (0, 0))
            screen.blit(start_button_image, (280, 210))
            screen.blit(change_music_button_image, (20, 40))

            music_text_script()
            screen.blit(music_text, (0, 0))

            if pygame.mouse.get_pressed()[0]:
                if click_cooldown == 0:
                    start_button()
                    change_music_button()

        def ingame_script():
            global spawn_cooldown

            screen.blit(background, (0, 0))

            if shop == False:
                if spawn_cooldown == 0:
                    spawn_omega_script()
                    spawn_cooldown = level_boxes_spawning * 80 - spawning_speed_level * 40
                    
                    global coins
                    coins += (level_boxes_spawning + spawning_speed_level) * 10

                else:
                    spawn_cooldown -= 1
                
                if pygame.mouse.get_pressed()[0]:
                    if click_cooldown == 0:
                        box_click()
                
                display_omegas_script()

                screen.blit(shop_button_image, (740, 530))

                
            
            elif shop == True:
                screen.blit(shop_background_image, (570, 0))
                screen.blit(close_button_image, (870, 570))
                
                screen.blit(faster_spawning_button_image, (580, 10))
                faster_spawning_costs_script()
                screen.blit(faster_spawning_costs_text, (748, 10))

                screen.blit(spawn_higher_boxes_button_image, (580, 92))
                spawn_higher_boxes_costs_script()
                screen.blit(spawn_higher_boxes_costs_text, (704, 92))
                
                screen.blit(give_away_coins_button_image, (580, 204))
                screen.blit(give_away_coins_text, (750, 204))

                screen.blit(get_coins_button_image, (580, 286))
                screen.blit(get_coins_text, (684, 286))

            display_coins_script()
            screen.blit(display_coins, (20, 550))

            if pygame.mouse.get_pressed()[0]:
                if click_cooldown == 0:
                    if shop == False:
                        shop_button()
                    
                    elif shop == True:
                        close_shop_button()
                        
                        faster_spawning_button()
                        spawn_higher_boxes_button()

        # Game

        screen.fill((0, 0, 0))
        pygame.display.update()

        time.sleep(1)

        screen.blit(this_game_is_not_my_invention, (0, 100))
        screen.blit(the_real_game_was_programmed_with_scratch_by_debiankaios, (0, 120))
        screen.blit(link_to_original_game, (0, 140))
        screen.blit(link_to_debiankaios, (0, 160))
        pygame.display.update()

        time.sleep(4)

        screen.fill((0, 0, 0))
        pygame.display.update()

        time.sleep(1)

        # Game Loop

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    exit_game()

            (cursor_x, cursor_y) = pygame.mouse.get_pos()

            ctrl_slash()
            
            developer_mode_script()

            screen.fill((0, 0, 0))
            
            if page == "main_menu":
                main_menu_script()

            if page == "ingame":
                ingame_script()

            cursor_script()

            ctrl_c()

            pygame.display.update()
            click_cooldown_script()

            time.sleep(1.0 / max_fps)
            clock.tick()

    except KeyboardInterrupt:
        exit_game()